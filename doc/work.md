# Actividades generales
Análisis y especificación de fuentes de datos y sistemas relacionados.

Componentes y procesos ETL a desarrollar para la extracción, transformación y carga de información.

Módulos de procesamiento de contratos a desarrollar para la extracción, conversión de formatos y traducción automática de información de plataformas de contratación públicas.

Elaboración y consolidación de productos de información.

# Fuentes de datos

Crear inventario inventario con la especificación de las fuentes disponibles, teniendo en cuenta al menos los siguientes aspectos:

- Formato(s)
- Tipo de acceso (e.g., HTTP, volcado de datos, servicios web).
- Modelo de datos.
- Frecuencia de actualización.
- Dimensionamiento.
- Idioma(s).


## ET2: Censo empresas del sector de la Ciberseguridad en España.
## ET3: Base de datos de empresas españolas del sector de la Ciberseguridad.
### Crawler Incibe: Empresas ciberseguridad
- Formato: warc y BBDD MongoDB
- Tipo de acceso: Dumps grupo UC3M [https://drive.google.com/drive/u/0/folders/1HGpYnBsR5E3otlDNICN7j1-2eUYD7FCj]
- Modelo de datos: Schema mongo (ver abajo)
- Frecuencia de actualización: TBD
- Dimensionamiento: ~ 150MB MongoDB bson
- Idioma(s): Castellano, TBD

#### Colecciones Mongo

* DownloadIncibe: Colección con datos descarga, HTML en ``db.metadata.findOne().startingpage``
* urlsIncibe: Colección con metadatos crawler: 
```json
{
        "_id" : ObjectId("5b3c943851e6a27a70916c70"),
        "url_idna" : "www.iberdatos.com",
        "crawler_status" : "ERROR",
        "error_code" : "None",
        "error_type" : "DNSLookupError",
        "datetime" : "2018-07-04 11:47:06.936446",
        "trial" : 1
}
```
#### WARC/WET
Formatos:

* WARC 1.0: Raw crawled data [https://iipc.github.io/warc-specifications/specifications/warc-format/warc-1.0/]
* WET: WET files which store extracted plaintext from the data stored in the WARC.

Dos ficheros por URL base:

* url.warc.gz: Formato Warc 1.0 
* url.wet.gz: Formato Warc 1.0

Ejemplos WARC y WET (anti-virus.es)
```html
WARC/1.0
WARC-Type: response
WARC-Record-ID: <urn:uuid:786f6db0-7f6d-11e8-a9b0-0cc47ac30eec>
WARC-Date: 2018-07-04T09:34:38Z
Content-Type: application/http; msgtype=response
WARC-Target-URI:: /
Content-Length: 49837
WARC-Payload-Digest: sha1:3e7676bd887ba6fe05c59f11caf6120afcffa5aa

<!DOCTYPE html>
... <!-- html doc root -->
</html>

WARC/1.0
WARC-Type: response
WARC-Record-ID: <urn:uuid:7d70570c-7f6d-11e8-a9b0-0cc47ac30eec>
WARC-Date: 2018-07-04T09:34:46Z
Content-Type: application/http; msgtype=response
WARC-Target-URI:: /aviso-legal
Content-Length: 50542
WARC-Payload-Digest: sha1:e0734d5db1d03e479e785181231f9f25a61d7d02

<!DOCTYPE html>
... <!-- html doc path /aviso-legal -->
</html>

```
#### ETL

lectura mongo --> lectura warc --> extractor información (TBD: modelo de datos) --> carga BBDD (TBD: modelo/tecnología) 

 
## ET4: Relación de dominios de empresas españolas del sector de la Ciberseguridad en los dominios .es y .com.
## ET5: Colección de contratos obtenidos por las plataformas de contratación pública.

### Contratación pública Española
- Formato: XML, esquema (http://contrataciondelestado.es/codice/)
- Tipo de acceso: ATOM, XML, DUMPS mensuales y anuales (http://www.hacienda.gob.es/es-ES/GobiernoAbierto/Datos%20Abiertos/Paginas/licitaciones_plataforma_contratacion.aspx)
- Modelo de datos: CÓDICE-PLACE (http://www.hacienda.gob.es/Documentacion/Publico/D.G.%20PATRIMONIO/Plataforma_Contratacion/especificacion_mecanismo_sindicacion.pdf)
- Frecuencia de actualización: ATOM y dumps mensuales
- Dimensionamiento: ~ 10MB zip
- Idioma: Castellano.

Descritos en:
http://datos.gob.es/es/catalogo/e04990101-licitaciones-publicadas-en-la-plataforma-de-contratacion-del-sector-publico

#### Formato y campos relevantes
```xml
<?xml version="1.0" encoding="utf8"?>
<feed xmlns="http://www.w3.org/2005/Atom">
    <entry>
        <id>URI licitacion</id>
        <title>Titulo max. 2000 caracteres</title>
        <!-- Datos formato CODICE -->
        <cac-place-ext:ContractFolderStatus>
            <!-- Objeto -->
            <cac:ProcurementProject>
                <cbc:Name> <!-- Path: cac-place-ext:ContractFolderStatus/cac:ProcurementProject/cbc:Name-->
                Objeto max. 2000 caracteres
                </cbc:Name>
            ...
                <!-- Clasificación CPV -->
                <cac:RequiredCommodityClassification> <!-- Path: cac-place-ext:ContractFolderStatus/cac:ProcurementProject/cac:RequiredCommodityClassification-->
                    <cbc:ItemClassificationCode listURI="http://contrataciondelestado.es/codice/cl/1.04/CPV2007-1.04.gc">79713000</cbc:ItemClassificationCode>
                </cac:RequiredCommodityClassification>
                <cac:RequiredCommodityClassification>
                    <cbc:ItemClassificationCode listURI="http://contrataciondelestado.es/codice/cl/1.04/CPV2007-1.04.gc">79712000</cbc:ItemClassificationCode>
                </cac:RequiredCommodityClassification>
                <!-- Others -->
                <!-- Subtipos cac:ProcurementProject/cbc:SubTypeCode -->
                
                <!-- PCA docs cac-place-ext:ContractFolderStatus/cac:LegalDocumentReference/cac:Attachment/cac:ExternalReference/cbc:URI -->
                
                <!-- PCA id cac-place-ext:ContractFolderStatus/cac:LegalDocumentReference/cbc:ID -->
                
                <!-- PCT docs cac-place-ext:ContractFolderStatus/cac:TechnicalDocumentReference/cac:Attachment/cac:ExternalReference/cbc:URI -->
                
                <!-- PCT id cac-place-ext:ContractFolderStatus/cac:TechnicalDocumentReference/cbc:ID -->
                
                <!-- NIF adjudicatario cac-place-ext:ContractFolderStatus/cac:TenderResult/cac:WinningParty/cac:PartyIdentification/cbc:ID -->
                
                <!-- Razon social adjudicatario cac-place-ext:ContractFolderStatus/cac:TenderResult/cac:WinningParty/cac:PartyName/cbc:Name -->
            </cac:ProcurementProject>
            
            
        </cac-place-ext:ContractFolderStatus>
    </entry>
</feed>

```
#### Caracterización contratos relacionados con Ciberseguridad, extracción de información y descarga

#### Selección de contratos relacionados

En función de la información disponible en los feed de la plataforma, se proponen los siguientes enfoques para la selección de contratos relacionados con Ciberseguridad:

1. Utilización de códigos CPV relacionados con Ciberseguridad: TODO: consultar si se elaborar/disponer de este listado.
2. Clasificación basada en palabras clave basada en una lista de términos de ciberseguridad: TODO: Estudiar fuentes disponibles.
Para esta clasificación se podrán utilizar los campos descripción y objeto cac-place y los PDF de pliegos.
3. Entrenamiento de un clasificador supervisado. Entrenador de un clasificador binario:
    A. Creación de gold-standar: Usando keywords y CPV.
    B. Entrenamiento en Biome.

#### Extracción de información

- Empresas adjudicatarias: NIF y objeto social. TODO: Estudiar casuística UTE.
- Título, Objeto y CPVs.

#### Descarga pliegos
Usando las propiedades ``cac:LegalDocumentReference``, ``cac:TechnicalDocumentReference``, ``cac:AditionalDocumentReference``.

#### ETL

# Cronograma y entregables

## Mes 1
ET1: Análisis y diseño técnico general de los diferentes componentes y procesos ETL de obtención, transformación y carga de los datos.
## Meses 2 y 3
ET6: Módulo de procesamiento de contratos incluyendo conversión de formatos, extracción de datos relevantes y traducción automática.
ET7: Integración a la plataforma de SESIAD de los datos de las plataformas de contratación pública española (PLACE), europea (TED) y latinoamericanas a través de un proceso ETL.
ET8: Catálogo de productos y servicios de Ciberseguridad INCIBE en formato base de datos e integrado en la plataforma de Vigilancia Sectorial de la SESIAD.

## Mes 4 (HITO 1)

EG2: Informe intermedio de progreso
EG4: Actas de reunión
ET1: Análisis y diseño técnico general de los diferentes componentes y procesos ETL de obtención, transformación y carga de los datos.
ET2: Censo empresas del sector de la Ciberseguridad en España.
ET3: Base de datos de empresas españolas del sector de la Ciberseguridad.
ET4: Relación de dominios de empresas españolas del sector de la Ciberseguridad en los dominios .es y .com.
ET5: Colección de contratos obtenidos por las plataformas de contratación pública.
ET10: Documentación técnica y descripción del proceso ETL.

## Mes 5
Continuación anteriores

## Mes 6 (HITO FINAL)

EG4: Actas de reunión
ET9: Código fuente y ejecutables de los procesos ETL a desarrollar para la extracción transformación y carga de datos.
ET10: Documentación técnica y descripción del proceso ETL.
ET11: Resumen descriptivo a publicar en la web del Plan TL (www.PlanTL.es) en el formato que se acuerde con INCIBE-SESIAD.



