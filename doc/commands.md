# Setup

## Requirements

- make
- python 3.6
- docker

## Commands

### Init python environment

```bash
make init
```

### Build docker images

```bash
make build
```

### Start dockerized services

```bash
make start
```
