import os
import subprocess
from datetime import datetime

import click


@click.command()
@click.option(
    "--output",
    default=".dumps",
    help="Output folder where zip files will be downloaded",
)
def download_files(output: str):

    if not os.path.exists(output):
        os.makedirs(output)

    fetch_file_command = "curl -XGET {url} > {output}"

    for file_url, file in fetch_url_dump_files():
        os.system(
            fetch_file_command.format(url=file_url, output="{}/{}".format(output, file))
        )


def fetch_url_dump_files():
    year, month, day = datetime.now().strftime("%Y-%m-%d").split("-")
    current_day_prefix = "{}{}{}".format(year, month, day)
    ftp_list_url = "ftp://guest:guest@ted.europa.eu/daily-packages/{year}/{month}".format(
        year=year, month=month
    )

    list_files_cmd = "curl --list-only {}/".format(ftp_list_url)

    files = subprocess.check_output(
        list_files_cmd.split(" "), universal_newlines=True
    ).split("\n")

    return [
        ("{ftp_url}/{file}".format(ftp_url=ftp_list_url, file=file), file)
        for file in files
        if file.startswith(current_day_prefix)
    ]


if __name__ == "__main__":
    download_files()
