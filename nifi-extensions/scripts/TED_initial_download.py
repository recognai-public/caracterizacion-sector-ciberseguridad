import os

import click
import pandas as pd


@click.command()
@click.option(
    "--output",
    default=".dumps",
    help="Output folder where zip files will be downloaded",
)
def download_dumps(output: str):

    if not os.path.exists(output):
        os.makedirs(output)

    wget_command_format = "curl -XGET {} | tar -xz --directory {}"
    url_format = "https://ted.europa.eu/xml-packages/monthly-packages/{year}/{year}-{month}.tar.gz"

    for date in pd.date_range(
        start="2011-01-01", periods=100, freq="M", normalize=True
    ):
        year, month = date.strftime("%Y-%m").split("-")
        url = url_format.format(year=year, month=month)
        os.system(wget_command_format.format(url, output))


if __name__ == "__main__":
    download_dumps()
