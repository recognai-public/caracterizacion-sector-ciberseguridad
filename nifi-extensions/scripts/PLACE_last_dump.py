import datetime
import os
import shutil
import tempfile

import click


@click.command()
@click.option(
    "--output",
    default=".dumps",
    help="Output folder where zip files will be downloaded",
)
def download_dumps(output: str):
    if not os.path.exists(output):
        os.makedirs(output)

    today = datetime.date.today()
    first = today.replace(day=1)
    last_month = (first - datetime.timedelta(days=1)).strftime("%Y%m")

    tmp = tempfile.mkdtemp()

    file_path = "licitacionesPerfilesContratanteCompleto3_{}.zip".format(last_month)
    link = "https://contrataciondelestado.es/sindicacion/sindicacion_643/{}".format(
        file_path
    )

    wget_command_format = "wget --no-check-certificate {} -P {} "
    os.system(wget_command_format.format(link, tmp))
    shutil.move("{}/{}".format(tmp, file_path), "{}/{}".format(output, file_path))


if __name__ == "__main__":
    download_dumps()
