import os
import shutil
import tempfile

import click
import requests
from bs4 import BeautifulSoup as BS


@click.command()
@click.argument("url", default="http://www.hacienda.gob.es/es-ES/GobiernoAbierto/Datos%20Abiertos/Paginas/licitaciones_plataforma_contratacion.aspx")
@click.option(
    "--output",
    default=".dumps",
    help="Output folder where zip files will be downloaded",
)
@click.option(
    "--href-prefix",
    default="https://contrataciondelestado.es/sindicacion/sindicacion_643/licitacionesPerfilesContratanteCompleto3",
    help="Keeping only href's with this prefix",
)
def download_dumps(url: str, output: str, href_prefix: str):

    if not os.path.exists(output):
        os.makedirs(output)

    wget_command_format = "wget --no-check-certificate {} -P {} "
    page = requests.get(url)
    soup = BS(page.text, "html.parser")

    links = [
        href
        for ul in soup.find_all("ul")
        for href in [a.attrs.get("href") for a in ul.find_all("a")]
        if href and href.startswith(href_prefix)
    ]

    tmp = tempfile.mkdtemp()

    for link in links:
        file_name = link.split("/")[-1]
        os.system(wget_command_format.format(link, tmp))
        shutil.move("{}/{}".format(tmp, file_name), "{}/{}".format(output, file_name))


if __name__ == "__main__":
    download_dumps()
