import ftplib
import os

import click


def traverse(ftp, depth=0, ouptput="data", prefix=None):
    """
    return a recursive listing of an ftp server contents (starting
    from the current directory)

    listing is returned as a recursive dictionary, where each key
    contains a contents of the subdirectory or None if it corresponds
    to a file.

    @param ftp: ftplib.FTP object
    """
    if depth > 10:
        return ["depth > 10"]
    level = {}
    for entry in (path for path in ftp.nlst() if path not in (".", "..")):
        try:
            ftp.cwd(entry)
            level[entry] = traverse(ftp, depth=depth + 1)
            ftp.cwd("..")
        except ftplib.error_perm:
            level[entry] = None
            if prefix and entry.startswith(prefix):
                with open(os.path.join(ouptput, entry), "wb") as file:
                    ftp.retrbinary(f"RETR {entry}", file.write)
    return level


@click.command(help="Download a serie of files from remote ftp server")
@click.option("--host", help="ftp server host", default="ftp.euipo.europa.eu")
@click.option("--user", help="fpt user", default="opendata")
@click.option("--passwd", help="ftp password", default="kagar1n")
@click.option(
    "--ftp-path",
    help="Remote ftp path where find files",
    default="/Trademark/Full/2019",
)
@click.option("--output", help="Folder where store downloaded files", default="data")
@click.option("--prefix", help="Prefix used for filter files", default=None)
def download_ftp_folder(
    host: str, user: str, passwd: str, ftp_path: str, output: str, prefix: str = None
):

    ftp = ftplib.FTP(host=host, user=user, passwd=passwd)
    ftp.connect()
    ftp.login(user, passwd)
    ftp.set_pasv(True)

    ftp.cwd(ftp_path)
    print(traverse(ftp, prefix=prefix, ouptput=output))
    ftp.close()


if __name__ == "__main__":
    download_ftp_folder()
