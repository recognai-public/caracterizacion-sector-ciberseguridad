#!/usr/bin/python3

import argparse
import http.client
import os
import csv
import json
import dateutil.parser
from base64 import b64encode


from elasticsearch import helpers, Elasticsearch
import csv


def main(
    file_path,
    delimiter,
    elastic_address,
    elastic_index,
    elastic_type,
    ssl,
    username,
    password,
    id_column,
):

    es = Elasticsearch([elastic_address])

    with open(file_path, encoding="ISO 8859-1") as f:
        reader = csv.DictReader(f, delimiter=delimiter)
        es.indices.delete(index=elastic_index, ignore=[400, 404])
        helpers.bulk(es, reader, index=elastic_index, doc_type=elastic_type)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="CSV to ElasticSearch.")

    parser.add_argument(
        "--elastic-address",
        required=False,
        type=str,
        default="localhost:9200",
        help="Your elasticsearch endpoint address",
    )
    parser.add_argument(
        "--ssl",
        dest="ssl",
        action="store_true",
        required=False,
        help="Use SSL connection",
    )
    parser.add_argument(
        "--username",
        required=False,
        type=str,
        help="Username for basic auth (for example with elastic cloud)",
    )
    parser.add_argument("--password", required=False, type=str, help="Password")
    parser.add_argument(
        "--csv-file", required=True, type=str, help="path to csv to import"
    )
    parser.add_argument(
        "--elastic-index",
        required=True,
        type=str,
        help="elastic index you want to put data in",
    )
    parser.add_argument(
        "--elastic-type",
        required=False,
        type=str,
        default="_doc",
        help="Your entry type for elastic",
    )
    parser.add_argument(
        "--id-column",
        type=str,
        default=None,
        help="If you want to have index and you have it in csv, this the argument to point to it",
    )
    parser.add_argument(
        "--delimiter",
        type=str,
        default=";",
        help="If you want to have a different delimiter than ;",
    )

    parsed_args = parser.parse_args()

    main(
        file_path=parsed_args.csv_file,
        delimiter=parsed_args.delimiter,
        elastic_index=parsed_args.elastic_index,
        elastic_type=parsed_args.elastic_type,
        elastic_address=parsed_args.elastic_address,
        ssl=parsed_args.ssl,
        username=parsed_args.username,
        password=parsed_args.password,
        id_column=parsed_args.id_column,
    )

