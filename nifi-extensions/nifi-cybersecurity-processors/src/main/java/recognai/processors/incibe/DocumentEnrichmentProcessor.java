package recognai.processors.incibe;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.apache.commons.lang3.StringUtils;
import org.apache.nifi.annotation.documentation.CapabilityDescription;
import org.apache.nifi.components.PropertyDescriptor;
import org.apache.nifi.expression.ExpressionLanguageScope;
import org.apache.nifi.flowfile.FlowFile;
import org.apache.nifi.processor.*;
import org.apache.nifi.processor.exception.ProcessException;
import org.apache.nifi.processor.util.StandardValidators;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.*;

@CapabilityDescription("Fetch an document url from a Flowfile attribute and extract its content content " +
        "into a new flowfile attribute. Python3 must be installed for a properly execution")
public class DocumentEnrichmentProcessor extends AbstractProcessor {

    public static final String OPERATION_OBJECT_TRANSLATION_OPERATION = "contract_object_translation";
    public static final String OPERATION_OBJECT_EXTRACT_OPERATION = "extract_contract_object";
    public static final String OPERATION_COMPANY_DOMAINS = "company_domains";

    private static final String ENV_ENRICHMENT_ENDPOINT = "ENRICHMENT_ENDPOINT";
    private static final String slash = "/";

    private ObjectMapper mapper = new ObjectMapper();

    static final PropertyDescriptor OPERATION_TYPE_ATTRIBUTE = new PropertyDescriptor.Builder()
            .name("operation")
            .displayName("Operation type")
            .description("Kind of attribute enrichment")
            .required(true)
            .allowableValues(OPERATION_OBJECT_TRANSLATION_OPERATION, OPERATION_OBJECT_EXTRACT_OPERATION, OPERATION_COMPANY_DOMAINS)
            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
            .build();

    static final PropertyDescriptor TO_ATTRIBUTE = new PropertyDescriptor.Builder()
            .name("to")
            .displayName("To attribute")
            .description("Attribute name used for save enrichment operation")
            .required(true)
            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
            // .expressionLanguageSupported(ExpressionLanguageScope.FLOWFILE_ATTRIBUTES)
            .build();


    static final Relationship REL_SUCCESS = new Relationship.Builder().name("success")
            .description("Successfully extract content from url document").build();
    static final Relationship REL_FAILURE = new Relationship.Builder().name("failure")
            .description("Failed to extract content from url document").build();


    private List<PropertyDescriptor> descriptors = Arrays.asList(
            OPERATION_TYPE_ATTRIBUTE,
            TO_ATTRIBUTE
    );
    private Set<Relationship> relationships = new HashSet<>(Arrays.asList(REL_SUCCESS, REL_FAILURE));
    private String enrichmentServiceEndpoint;

    @Override
    public Set<Relationship> getRelationships() {
        return this.relationships;
    }

    @Override
    public final List<PropertyDescriptor> getSupportedPropertyDescriptors() {
        return descriptors;
    }


    @Override
    protected void init(ProcessorInitializationContext context) {
        super.init(context);
        String endpoint = System.getenv(ENV_ENRICHMENT_ENDPOINT);
        this.enrichmentServiceEndpoint = StringUtils.endsWith(endpoint, slash) ? endpoint : endpoint + slash;
    }

    @Override
    protected PropertyDescriptor getSupportedDynamicPropertyDescriptor(final String propertyDescriptorName) {
        return new PropertyDescriptor.Builder()
                .name(propertyDescriptorName)
                .expressionLanguageSupported(ExpressionLanguageScope.NONE)
                .addValidator(StandardValidators.NON_EMPTY_VALIDATOR) // TODO
                .required(false)
                .dynamic(true)
                .build();
    }

    @Override
    public void onTrigger(ProcessContext context, ProcessSession session) throws ProcessException {
        final FlowFile flowFile = session.get();
        if (flowFile == null) {
            return;
        }
        String operationType = null, request = null;

        try {
            operationType = context.getProperty(OPERATION_TYPE_ATTRIBUTE).getValue();
            request = requestPayload(context, flowFile);
            String result;

            switch (operationType.toLowerCase().trim()) {
                case OPERATION_OBJECT_TRANSLATION_OPERATION:
                    result = objectTranslation(request);
                    break;
                case OPERATION_OBJECT_EXTRACT_OPERATION:
                    result = objectExtraction(request);
                    break;
                case OPERATION_COMPANY_DOMAINS:
                    result = companyDomains(request);
                    break;
                default:
                    throw new Exception(String.format("Wrong operation type: $s", operationType));
            }
            String toAttribute = context.getProperty(TO_ATTRIBUTE).getValue();
            session.putAttribute(flowFile, toAttribute, result);
            session.transfer(flowFile, REL_SUCCESS);
            session.commit();
        } catch (Exception e) {
            String errorMessage = String.format(
                    "Cannot process operation [%s] with parameters [%s]. Error: [%s]",
                    operationType,
                    request,
                    e.getMessage()
            );
            getLogger().error(errorMessage);
            session.transfer(flowFile, REL_FAILURE);
        }
    }

    private String requestPayload(ProcessContext context, FlowFile flowFile) throws JsonProcessingException {
        Map<String, String> data = new HashMap<>();
        for (final Map.Entry<PropertyDescriptor, String> entry : context.getProperties().entrySet()) {
            if (!entry.getKey().isDynamic()) {
                continue;
            }
            data.put(entry.getKey().getName(), flowFile.getAttribute(entry.getValue()));
        }
        return mapper.writeValueAsString(data);
    }

    private String companyDomains(String requestData) throws UnirestException {

        JSONArray domainsResponse = Unirest.post(this.enrichmentServiceEndpoint + "company-domains")
                .header("Content-Type", "application/json")
                .body(requestData)
                .asJson()
                .getBody()
                .getArray();

        List<String> domains = new ArrayList<String>();

        if (domainsResponse != null) {
            int len = domainsResponse.length();
            for (int i = 0; i < len; i++) {
                domains.add(domainsResponse.get(i).toString());
            }
        }

        return String.join(",", domains);
    }

    private String objectExtraction(String requestData) throws UnirestException, JsonProcessingException {
        JSONObject response = Unirest.post(this.enrichmentServiceEndpoint + "document-object-extract")
                .header("Content-Type", "application/json")
                .body(requestData)
                .asJson()
                .getBody()
                .getObject();

        return getSafetyProperty(response, "object");
    }

    private String objectTranslation(String requestData) throws UnirestException, JsonProcessingException {

        JSONObject response = Unirest.post(this.enrichmentServiceEndpoint + "text-translation")
                .header("Content-Type", "application/json")
                .body(requestData)
                .asJson()
                .getBody()
                .getObject();

        return getSafetyProperty(response, "translated");
    }

    private String getSafetyProperty(JSONObject data, String propertyKey) {
        return data.has(propertyKey) ? data.getString(propertyKey) : "";
    }

}
