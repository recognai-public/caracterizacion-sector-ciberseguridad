package recognai.processors.incibe;

import org.apache.nifi.util.MockFlowFile;
import org.apache.nifi.util.StringUtils;
import org.apache.nifi.util.TestRunner;
import org.apache.nifi.util.TestRunners;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DocumentEnrichmentProcessorTest {

    private TestRunner testRunner;

    @Before
    public void init() {
        testRunner = TestRunners.newTestRunner(DocumentEnrichmentProcessor.class);
    }

    @Test
    public void testObjectExtractionProcessor() {

        Map<String, String> attributes = new HashMap<>();

        String uriProperty = "uri";
        String fileUriAttribute = "file.url";
        String outputAttribute = "file.content";

        attributes.put(fileUriAttribute, "http://contrataciondelestado.es/wps/wcm/connect/PLACE_es/Site/area/docAccCmpnt?srv=cmpnt&cmpntname=GetDocumentsById&source=library&DocumentIdParam=dbf15f5e-121b-4471-ad57-ead54a7c0bb9");

        testRunner.setProperty(
                DocumentEnrichmentProcessor.OPERATION_TYPE_ATTRIBUTE,
                DocumentEnrichmentProcessor.OPERATION_OBJECT_EXTRACT_OPERATION
        );

        testRunner.setProperty(DocumentEnrichmentProcessor.TO_ATTRIBUTE, outputAttribute);
        testRunner.setProperty(uriProperty, fileUriAttribute);

        testRunner.enqueue("Mock content", attributes);
        testRunner.run();

        List<MockFlowFile> flowFiles = testRunner.getFlowFilesForRelationship(DocumentEnrichmentProcessor.REL_SUCCESS);

        flowFiles.forEach(file -> {
            Assert.assertFalse(StringUtils.isEmpty(file.getAttribute(outputAttribute)));
        });

        testRunner.getFlowFilesForRelationship(DocumentEnrichmentProcessor.REL_SUCCESS);
    }

    @Test
    public void testCompanyDomain() {

        Map<String, String> attributes = new HashMap<>();

        String nameProperty = "name";
        String idProperty = "id";
        String companyNameAttribute = "company.name";
        String companyIdAttribute = "company.id";
        String outputAttribute = "domains";

        attributes.put(companyIdAttribute, "B3333333");
        attributes.put(companyNameAttribute, "Mercadona");

        testRunner.setProperty(
                DocumentEnrichmentProcessor.OPERATION_TYPE_ATTRIBUTE,
                DocumentEnrichmentProcessor.OPERATION_COMPANY_DOMAINS
        );

        testRunner.setProperty(DocumentEnrichmentProcessor.TO_ATTRIBUTE, outputAttribute);
        testRunner.setProperty(idProperty, companyIdAttribute);
        testRunner.setProperty(nameProperty, companyNameAttribute);

        testRunner.enqueue("Mock content", attributes);
        testRunner.run();

        List<MockFlowFile> flowFiles = testRunner.getFlowFilesForRelationship(DocumentEnrichmentProcessor.REL_SUCCESS);

        flowFiles.forEach(file -> {
            Assert.assertFalse(StringUtils.isEmpty(file.getAttribute(outputAttribute)));
        });

        testRunner.getFlowFilesForRelationship(DocumentEnrichmentProcessor.REL_SUCCESS);
    }

    @Test
    public void testTranslation() {
        Map<String, String> attributes = new HashMap<>();

        String originalTextProperty = "original";
        String originalTextAttribute = "text";
        String translatedAttribute = "translated";

        attributes.put(originalTextAttribute, "");

        testRunner.setProperty(
                DocumentEnrichmentProcessor.OPERATION_TYPE_ATTRIBUTE,
                DocumentEnrichmentProcessor.OPERATION_OBJECT_TRANSLATION_OPERATION
        );

        testRunner.setProperty(DocumentEnrichmentProcessor.TO_ATTRIBUTE, translatedAttribute);
        testRunner.setProperty(originalTextProperty, originalTextAttribute);

        testRunner.enqueue("Mock content", attributes);
        testRunner.run();

        List<MockFlowFile> flowFiles = testRunner.getFlowFilesForRelationship(DocumentEnrichmentProcessor.REL_SUCCESS);

        flowFiles.forEach(file -> {
            Assert.assertFalse(StringUtils.isEmpty(file.getAttribute(translatedAttribute)));
        });

        testRunner.getFlowFilesForRelationship(DocumentEnrichmentProcessor.REL_SUCCESS);
    }

}