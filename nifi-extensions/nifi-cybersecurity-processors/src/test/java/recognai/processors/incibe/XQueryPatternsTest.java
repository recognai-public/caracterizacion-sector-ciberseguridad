/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package recognai.processors.incibe;

import org.apache.nifi.processors.standard.EvaluateXPath;
import org.apache.nifi.processors.standard.EvaluateXQuery;
import org.apache.nifi.util.MockFlowFile;
import org.apache.nifi.util.TestRunner;
import org.apache.nifi.util.TestRunners;
import org.junit.Before;
import org.junit.Test;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;


public class XQueryPatternsTest {

    private TestRunner testRunner;

    private String placeXML = Thread.currentThread().getContextClassLoader().getResource("entry.xml").getPath();
    private String tedXML = Thread.currentThread().getContextClassLoader().getResource("ted.data.xml").getPath();

    @Before
    public void init() {
        testRunner = TestRunners.newTestRunner(EvaluateXQuery.class);
    }

    @Test
    public void testPlacePatterns() throws FileNotFoundException {
        testRunner.setProperty(EvaluateXQuery.DESTINATION, EvaluateXQuery.DESTINATION_ATTRIBUTE);
        testRunner.setProperty(EvaluateXQuery.VALIDATE_DTD, "false");
        testRunner.setProperty(EvaluateXQuery.XML_OUTPUT_METHOD, EvaluateXQuery.OUTPUT_METHOD_TEXT);

        testRunner.setProperty("CPV", "//*:ItemClassificationCode");
        testRunner.setProperty("contractId", "//*:id");
        testRunner.setProperty("contractTitle", "//*:title");
        testRunner.setProperty("winningPartyId", "//*:WinningParty/*:PartyIdentification/*:ID");
        testRunner.setProperty("winningPartyName", "//*:WinningParty/*:PartyName/*:Name");
        testRunner.setProperty("contractDocumentationURI", "//*:LegalDocumentReference/descendant::*:URI");

        testRunner.enqueue(new FileInputStream(this.placeXML));
        testRunner.run();

        List<MockFlowFile> flowFiles = testRunner.getFlowFilesForRelationship(EvaluateXPath.REL_MATCH);
        flowFiles.stream().forEach(file -> System.out.println(file.getAttributes()));
    }

    @Test
    public void testReadTEDPublications() throws FileNotFoundException {
        testRunner.setProperty(EvaluateXQuery.DESTINATION, EvaluateXQuery.DESTINATION_ATTRIBUTE);
        testRunner.setProperty(EvaluateXQuery.VALIDATE_DTD, "false");
        testRunner.setProperty(EvaluateXQuery.XML_OUTPUT_METHOD, EvaluateXQuery.OUTPUT_METHOD_TEXT);

        testRunner.setProperty("isNewFormat", "boolean(//*:OBJECT_CONTRACT/*:SHORT_DESCR)");
        testRunner.setProperty("isOldFormat", "boolean(//*:DESCRIPTION_AWARD_NOTICE_INFORMATION/*:TITLE_CONTRACT)");

        testRunner.setProperty("CPV", "//*:ORIGINAL_CPV/@CODE");
        testRunner.setProperty("documentType", "//*:TD_DOCUMENT_TYPE/@CODE");
        testRunner.setProperty("languages", "tokenize(//*:FORM_LG_LIST, ' ')");

        testRunner.setProperty("contractTitle.old.es", "//*:DESCRIPTION_AWARD_NOTICE_INFORMATION/*:TITLE_CONTRACT[ancestor::*[@LG='ES']]");
        testRunner.setProperty("contractTitle.old.en", "//*:DESCRIPTION_AWARD_NOTICE_INFORMATION/*:TITLE_CONTRACT[ancestor::*[@LG='EN']]");
        testRunner.setProperty("contractTitle.new.es", "//*:OBJECT_CONTRACT/*:TITLE[ancestor::*[@LG='ES']]");
        testRunner.setProperty("contractTitle.new.en", "//*:OBJECT_CONTRACT/*:TITLE[ancestor::*[@LG='EN']]");

        testRunner.setProperty("contractDescription.new.es", "//*:OBJECT_CONTRACT/*:SHORT_DESCR[ancestor::*[@LG='ES']]");
        testRunner.setProperty("contractDescription.new.en", "//*:OBJECT_CONTRACT/*:SHORT_DESCR[ancestor::*[@LG='EN']]");
        testRunner.setProperty("contractDescription.old.es", "//*:DESCRIPTION_AWARD_NOTICE_INFORMATION/*:SHORT_CONTRACT_DESCRIPTION[ancestor::*[@LG='ES']]");
        testRunner.setProperty("contractDescription.old.en", "//*:DESCRIPTION_AWARD_NOTICE_INFORMATION/*:SHORT_CONTRACT_DESCRIPTION[ancestor::*[@LG='EN']]");

        testRunner.setProperty("winningPartyName.old", "//*:ECONOMIC_OPERATOR_NAME_ADDRESS/*/*:ORGANISATION[ancestor::*[@CATEGORY='ORIGINAL']]");
        testRunner.setProperty("winningPartyName.new", "//*:CONTRACTOR/*:ADDRESS_CONTRACTOR/*:OFFICIxALNAME[ancestor::*[@CATEGORY='ORIGINAL']]");

        testRunner.setProperty("winningPartyAddress.new", "//*:CONTRACTOR/*:ADDRESS_CONTRACTOR/*[ancestor::*[@CATEGORY='ORIGINAL']]");
        testRunner.setProperty("winningPartyId.new", "//*:CONTRACTOR/*:ADDRESS_CONTRACTOR/*:NATIONALID/*[ancestor::*[@CATEGORY='ORIGINAL']]");
        testRunner.setProperty("winningPartyAddress.old", "//*:ECONOMIC_OPERATOR_NAME_ADDRESS/*:CONTACT_DATA_WITHOUT_RESPONSIBLE_NAME[ancestor::*[@CATEGORY='ORIGINAL']]");


        testRunner.enqueue(new FileInputStream(tedXML));
        testRunner.run();

        List<MockFlowFile> flowFiles = testRunner.getFlowFilesForRelationship(EvaluateXPath.REL_MATCH);
        flowFiles.stream().forEach(file -> System.out.println(file.getAttributes()));
    }

}
