#!/usr/bin/env python
# -*- coding: utf-8 -*-
from setuptools import setup, find_packages

if __name__ == "__main__":
    setup(
        name="enrichment-service",
        description="Enrichment service por contract and company information",
        author="Recognai",
        author_email="francisco@recogn.ai",
        url="https://recogn.ai/",
        long_description=open("README.md").read(),
        long_description_content_type="text/markdown",
        packages=find_packages("src"),
        package_dir={"": "src"},
        install_requires=[
            "click>=7.0,<8.0",
            "tika>=1.0,<2.0",
            "requests>=2.0,<3.0",
            "beautifulsoup4>=4.7,<4.8",
            "lxml~=4.3.0",
            "Flask>=1.0,<2.0",
            "gevent>=1.4.0,<1.5.0",
            "pandas>=0.24,<0.25",
            # OpenNMT-py dependencies
            "opennmt-py@git+https://github.com/OpenNMT/OpenNMT-py",
            "ConfigArgParse",
            "torch==1.1",
            "torchtext@git+https://github.com/pytorch/text.git@9902e79",
            "spacy~=2.1",
        ],
        extras_require={"testing": ["pytest", "pytest-cov", "pytest-pylint"]},
        entry_points={"console_scripts": ["enrichment.serve=enrichment.cli:serve"]},
        python_requires=">=3.6.1",
        zip_safe=False,
    )
