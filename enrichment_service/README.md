# Contract information enrichment

This module allow extract/append some information for document contracts information

## Requirements

- python 3.6

## Configuration

You can configure a python virtual environment using python3 venv module:

```bash
python3 -m venv .venv && source .venv/bin/activate
```

## Running the server

Just install python module:

```bash
pip install .
```

and launch the serve command:

```bash
enrichment.serve --port 8888
```

the argument --domains points to the domain list, and can be downloaded from https://sede.red.gob.es/procedimientos/solicitud-acceso-listado-dominios-risp

## Enrichments operations

Several operations are supported by this service and explained in next sections.

### Contract Object extraction

```bash
curl -X POST \
http://localhost:8888/document-object-extract \
-H 'Content-Type: application/json' \
-d '{
    "uri": "https://contrataciondelestado.es/wps/wcm/connect/PLACE_es/Site/area/docAccCmpnt?srv=cmpnt&cmpntname=GetDocumentsById&source=library&DocumentIdParam=82f65af7-2451-4987-8dfb-6af584faadd0"
}'
```

### Object translation

```bash
curl -X POST \
http://localhost:8888/text-translation \
-H 'Content-Type: application/json' \
-d '{
    "text": "this is an example"
}'
```

### Company domains extraction

```bash
curl -X POST \
http://localhost:8888/company-domains \
-H 'Content-Type: application/json' \
-d '{
    "name": "My company",
    "id": "A333449494"
}'
```
