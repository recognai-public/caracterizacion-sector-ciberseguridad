import io
import logging
import re
from typing import Dict, List, Optional
from urllib.parse import urlparse

import onmt.opts as opts
import pandas
import requests
import spacy
from bs4 import BeautifulSoup as BS
from onmt.translate import Translator
from onmt.translate.translator import build_translator
from onmt.utils.parse import ArgumentParser
from pandas import DataFrame
from spacy.matcher import Matcher
from spacy.tokens import Doc
from tika import parser

logger = logging.getLogger(__name__)
logging.getLogger("tika.tika").setLevel(logging.CRITICAL)
logger.setLevel(logging.INFO)


def spacy_load(name: str, **kwargs):
    """
    Wraps spacy.load method allowing download model if doesn't exist

    :param name: the model name
    :param kwargs: extra argument passed to spacy.load method
    :return:
    The loaded spaCy language
    """
    logger.info('Loading "%s" spaCy language models', name)

    try:
        return spacy.load(name, **kwargs)
    except OSError:
        from spacy.cli import download

        download(name)
        return spacy.load(name, **kwargs)


class ContractInformationEnrichmentService(object):
    _DOMAINS_ID_REPLACEMENT_PATTERN = "[^0-9a-zA-Z]+"
    _UNNEEDED_BUT_REQUIRED_PREDICTIONS_FILE = "predictions.txt"
    _OBJETO_DEL_CONTRATO_MATCH = "ObjetoDelContrato"
    _CONTRATO_TIENE_COMO_OBJETO_MATCH = "ContratoTienePorObjeto"
    _SPACY_LANG = "es"
    _MINIMAL_OBJETO_DESCRIPTION_SIZE = 25

    def __init__(self, domains_path: str = None, translation_model: str = None):
        self._domains_df = self._process_domains(domains_path)
        self._translator = self._configure_translator(translation_model)
        self.nlp = spacy_load(self._SPACY_LANG)
        self.spacy_matcher = self._configure_spacy_matcher()

    def _configure_spacy_matcher(self):
        matcher = Matcher(self.nlp.vocab)
        matcher.add(
            self._OBJETO_DEL_CONTRATO_MATCH,
            None,
            *[
                [
                    {"LOWER": "objeto"},
                    {"LOWER": "del"},
                    {"LOWER": "contrato"},
                ],  # Match "Objeto del contrato"
                [
                    {"LOWER": "objeto"},
                    {"LOWER": ":"},
                    {"OP": "+"},
                ],  # Match "object: blalbll blaaba"
                [
                    {"LOWER": {"IN": ["objeto"], "DEP": {"IN": ["nsubj", "conj"]}}},
                    {"OP": "?"},
                    {"OP": "?"},
                    {
                        "LOWER": {
                            "IN": ["contrato", "pliego", "contratación", "licitación"],
                            "DEP": {"IN": ["obj", "nmod"]},
                        }
                    },
                ],  # Match "El objeto del presente contrato...."
            ]
        )
        matcher.add(
            self._CONTRATO_TIENE_COMO_OBJETO_MATCH,
            None,
            *[
                [
                    {"LEMMA": "tener"},
                    {"OP": "?"},
                    {"LOWER": {"IN": ["objeto"], "DEP": "obj"}},
                ]  # Match "El contrato presente tiene como objeto..."
            ]
        )
        return matcher

    def document_object_extract(self, uri: str, first_pages: int = 5) -> Dict[str, str]:
        """
        Given an contract legal documentation by its uri, extract textual content related to contract object
        :param uri: the contract legal uri
        :param first_pages: filter the object content to this set of pages
        :return: A dictionary with one single key (object) containing related object contract information
        """
        uri = (uri or "").strip()
        if len(uri) == 0:
            return dict(object="")

        document_uri = self._fetch_document_url(uri)
        content = parser.from_file(document_uri, xmlContent=True)

        html = BS(content.get("content", ""), "lxml")
        pages = html.find_all("div", {"class": "page"})

        text_blocks = [
            p.text
            for page in pages[:first_pages]
            for p in page.find_all("p")
            if p.text and len(p.text) > 1
        ]

        objeto_texts = []
        for doc in self.nlp.pipe(text_blocks):
            if self._match_as_objeto(doc):
                objeto_texts.append(doc.text)

        return dict(object="".join(objeto_texts))

    @staticmethod
    def _fetch_document_url(uri):
        page = requests.get(uri)
        html = BS(page.text, "html.parser")
        meta_refresh = html.find("meta", {"http-equiv": "refresh"})
        if meta_refresh:
            parsed_uri = urlparse(uri)
            try:
                _, content_url = meta_refresh.attrs.get("content").split(";")
                content_url = content_url.replace("url=", "")[1:-1]

                uri = "{uri.scheme}://{uri.netloc}{context}".format(
                    uri=parsed_uri, context=content_url
                )
            except Exception as e:
                logger.warning(e)
        return uri

    def text_translation(self, text: str) -> Dict[str, str]:
        """
        Makes a text translation from english to spanish

        :param text: the original text
        :return: A dictionary containing the translated text content (translated key)
        """
        text = (text or "").strip()
        if len(text) == 0:
            return dict(translated=text)

        _, out = self._translator.translate(src=[text], batch_size=1)

        return dict(translated=out[0][0])

    def company_domains(self, name: str = None, id: str = None) -> List[str]:
        """
        Given a company name and id (CIF, for example) try to fetch the company registered domains
        :param name: the company name
        :param id: the company id (National Identification Code)
        :return: a list company registered domains
        """

        domains = self._find_domains_by_id(id)
        if domains and len(domains) > 0:
            return domains

        return self._find_domains_by_name(name)

    def _process_domains(self, domains_path: str) -> Optional[DataFrame]:
        try:
            df = pandas.read_csv(
                domains_path,
                sep="|",
                error_bad_lines=False,
                names=["domain", "name", "id"],
            )

            df["id"] = df["id"].str.replace(self._DOMAINS_ID_REPLACEMENT_PATTERN, "")
            df["name"] = df["name"].str.lower()

            return df
        except Exception as e:
            logger.warning(
                "Cannot load domains {}. This functionallity will be disabled".format(e)
            )
            return None

    def _find_domains_by_id(self, id: str) -> List[str]:

        if not id or id.strip().isspace():
            return []
        id = re.sub(self._DOMAINS_ID_REPLACEMENT_PATTERN, "", id.strip())
        filtered_df = self._domains_df[self._domains_df["id"] == id]

        return self._domains_from_dataframe(filtered_df)

    def _find_domains_by_name(self, name: str):
        if not name:
            return []

        name = name.lower()
        filtered_df = self._domains_df[self._domains_df["name"] == name]
        return self._domains_from_dataframe(filtered_df)

    @staticmethod
    def _domains_from_dataframe(filtered_df):
        return filtered_df["domain"].to_list()

    def _configure_translator(self, model: str) -> Translator:
        def _get_parser():
            p = ArgumentParser(description="translate")

            opts.config_opts(p)
            opts.translate_opts(p)
            return p

        # We need use the same argument options namespace since
        # the high code coupling to translator initialization
        arg_parser = _get_parser()
        opt = arg_parser.parse_args(
            args=[
                "--model",
                model,
                "--data_type",
                "text",
                "--src",
                "none",
                "--replace_unk",
            ]
        )

        return build_translator(opt, report_score=False, out_file=NoopStringIO())

    def _match_as_objeto(self, doc: Doc) -> bool:
        matches = self.spacy_matcher(doc)
        if len(matches) > 0 and len(doc) > self._MINIMAL_OBJETO_DESCRIPTION_SIZE:
            for match_id, start, end in matches:
                match_name = self.nlp.vocab.strings[match_id]
                if match_name == self._OBJETO_DEL_CONTRATO_MATCH and start < 10:
                    return True
                elif match_name == self._CONTRATO_TIENE_COMO_OBJETO_MATCH:
                    return True
        return False


class NoopStringIO(io.StringIO):
    def write(self, text):
        pass

    def flush(self):
        pass
