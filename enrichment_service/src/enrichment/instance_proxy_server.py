import json
import logging

from flask import Flask
from flask import request
from gevent.pywsgi import WSGIServer


class InstanceProxyServer:
    _logger = logging.getLogger(__name__)
    """
    This class allows to expose a instance class through a rest http API
    """

    def __init__(self, instance: object):
        """
        :param instance: the service instance
        """
        assert instance, "Cannot serve a None instance"
        self.app = Flask(instance.__class__.__name__)
        self._configure_app(instance, self.app)

    def serve(self, port: int = 8888) -> None:
        """
            Serve the instance object as a rest server and block until finish
        """

        http_server = WSGIServer(("0.0.0.0", port), self.app)
        self._logger.info(f"Model loaded, serving on port {port}")
        http_server.serve_forever()

    def _configure_app(self, instance, flask_app: Flask):
        """
        Expose service handler via REST attending at his method names
        
        :param instance the service instance
        :param handler_method_message
        :param flask_app the flask app

        """

        _handler = instance

        # TODO include a way for describe service, status and so on

        @flask_app.route("/<string:action>", methods=["GET", "POST"])
        def handler(action):
            input = {} if request.method == "GET" else json.loads(request.data)
            handler_method = getattr(_handler, action.lower().replace("-", "_"))
            result = handler_method(**input)
            return json.dumps(result)
