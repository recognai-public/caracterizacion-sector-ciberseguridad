import click

from .instance_proxy_server import InstanceProxyServer
from .service import ContractInformationEnrichmentService


@click.command(
    "enrichment.serve", help="serve methods for to contract information extraction"
)
@click.option("--port", help="port used for serving", default=8888)
@click.option(
    "--domains",
    help="path reference to domains path .bz2 file",
    default="domains.csv.bz2",
)
@click.option(
    "--translation-model",
    help="path reference to translation model path",
    default="model.pt",
)
def serve(port: int, domains: str = None, translation_model: str = None) -> None:

    service = ContractInformationEnrichmentService(
        domains_path=domains, translation_model=translation_model
    )

    server = InstanceProxyServer(service)

    click.echo(f"Running service on port {port}...")
    server.serve(port=port)
