# Servicios de obtención y clasificación de información para la caracterización del sector de la ciberseguridad

Este documento describe la implementación de procesos para la obtención y clasificación de información para la caracterización de empresas y contratos públicos en el sector de la ciberseguridad

# Arquitectura general del sistema

Los procesos desarrollados para la realización de esta tarea se basan en la herramienta de definición de flujo de datos [Apache Nifi](https://nifi.apache.org/). Además, estos procesos se apoyan en un servicio desarrollado a tal efecto para algunas tareas más complejas, como la extracción del objeto del contrato, la traducción automática o la consulta en el registro de dominios

El resultado de todos estos procesos de transformación de datos es persistido en [elasticsearch](https://www.elastic.co/es/products/elasticsearch), que permite un acceso simple a los datos y un dsl de consulta bajo peticiones http

La figura siguiente trata de mostrar una visión global del sistema:

![](images/arquitectura.png)

# Instalación

La sección siguiente define cómo instalar y configurar los servicios para la obtención y clasificación de información para la caracterización del sector de la ciberseguridad.

## Requisitos de instalación

Los servicios para obtención y clasificación de información para la caracterización del sector de la ciberseguridad tienen como requisitos los siguientes componentes/lenguajes:

- **Apache Nifi 1.9.x** para la definición de flujos de datos
- **[Docker 18.09](http://www.docker.com)** como entorno de ejecución de los servicios
- **[elasicsearch 6.6.x](https://www.elastic.co/es/products/elasticsearch)** como sistema de persistencia para
- **Python 3.6** para el desarrollo del servicio de enriquecimiento (traducción automática, extracción de objeto de contrato, registro de dominios)
- **Java 1.8** para el desarrollo processors NiFi necesarios en el flujo de datos

## Estructura del repositorio

El repositorio está estructurado de la siguiente forma

    .
    |-- enrichment_service
    |   |-- src
    |   |-- Dockerfile
    |   |-- README.md
    |   |-- domains.csv.bz2
    |   |-- setup.cfg
    |   `-- setup.py
    |-- nifi-extensions
    |   |-- nifi-cybersecurity-nar
    |   |-- nifi-cybersecurity-processors
    |   |-- scripts
    |   |-- Dockerfile
    |   `-- pom.xml
    |-- runtime-volumes
    |   |-- elasticsearch
    |   `-- nifi
    |-- sample-data
    |   |-- incibe
    |   |-- mx
    |   |-- place
    |   `-- ted
    |-- docker-compose.yml
    |-- Makefile
    |-- ARCHITECTURE.md
    |-- README.md
    `-- setup.md

siendo los módulos principales los siguientes:

- `enrichment_service`: código fuente del servicio de enriquecimiento. Además define un fichero de configuración para empaquetar el módulo como una imagen de docker.
- `nifi-extensions`: nifi processors desarrollados para la integración del servicio de enriquecimiento en el flujo de datos de Apache NiFi. Además define un fichero de configuración para empaquetar una distribución de NiFi, incluyendo los processors, como una imagen de docker.
- `runtime-volumes`: sistema de ficheros y configuraciones necesarios para la ejecución del servicio completo en un entorno _dockerizado_
- `sample-data`: algunos ejemplos de fuentes de datos para testear el sistema

La configuración de ejecución en un entorno dockerizado está definida en el fichero `docker-compose.yml`. Este fichero contiene la definición de los distintos procesos necesarios para el servicio:

- Apache NiFi
- Elasticsearch
- Apache Tika server
- Servicio de enriquecimiento

## Guía de comandos de ejecución

Se proporciona un fichero `Makefile` con las directivas necesarias para configurar y ejecutar el servicio en un entorno docker, y que se detallan a continuación:

    build                          Build the needed docker images (depends on package directive)
    init                           Prepare a python virtual environments, install dependencies an so on. Do it just once
    logs                           Attach services logs
    package                        Build custom nifi procesors
    start                          Launch local services
    status                         Check services status
    stop                           Stop local services

`init`: configura un entorno virtual en la carpeta `.venv`. Este comando sólo debería ejecutar una única vez.

`package`: compila y empaqueta los processors nifi desarrollados.

`build`: construye las imágenes docker necesarias (**nifi+processors** y **servicio de enriquecimiento**). Esta directiva depende de la directiva `package`, que será ejecutada automáticamente

`start`: arranca los servicios docker

`status`: muestra el estado de los servicios

`stop`: para los servicios docker

## Arranque inicial del servicio

Para lanzar el servicio por primera vez necesario configurar el entorno virtual de python y construir las imágenes de docker necesarias, es por eso que haremos uso de las directivas `init` y `build`

    make init build start

Una vez arrancado, podemos comprobar el estado de los servicios

    make status

    Name                      Command               State                      Ports
    ----------------------------------------------------------------------------------------------------------
    etl_elasticsearch_1   /usr/local/bin/docker-entr ...   Up      0.0.0.0:9200->9200/tcp, 9300/tcp
    etl_enrichment_1      /bin/sh -c enrichment.serv ...   Up      0.0.0.0:8888->8888/tcp
    etl_nifi_1            ../scripts/start.sh              Up      10000/tcp, 0.0.0.0:8080->8080/tcp, 8443/tcp
    etl_tika-server_1     /bin/sh -c java -jar /tika ...   Up      0.0.0.0:9998->9998/tcp

si todos los servicios tienen el estado `Up`, podremos visitar la url [http://localhost:8080/nifi](http://localhost:8080/nifi/) para verificar los flujos de datos configurados.

![](images/overal.nifi.flow.png)

Siempre se puede usar la directiva stop para parar los servicios

    make stop

    Stopping etl_nifi_1          ... done
    Stopping etl_enrichment_1    ...
    Stopping etl_elasticsearch_1 ... done
    Stopping etl_tika-server_1   ...

# Flujos ELT en Apache NiFi

A continuación detallaremos los distintos flujos de datos definidos en la plataforma Apache NiFi. Cada flujo lee de una carpeta local diferente, permitiendo alimentarlos de forma manual, o mediante scripts de volcado de datos a esas carpetas.

La información extraída de los flujos se persiste en **elasticsearch**, un motor de búsqueda textual que permite el acceso mediante un Api RESt. La información persistida se divide en dos [índices](https://www.elastic.co/guide/en/elasticsearch/reference/current/getting-started-concepts.html#_index): `companies` y `contracts` y su modelo de datos lo podemos ver a continuación

    # ejemplo de datos de empresa
    {
      "company.integrator" : "",
      "company.country" : "España",
      "company.province" : "Lugo",
      "company.distributor" : "No",
      "company.domains" : "",
      "company.manufacturer" : "No",
      "company.website" : "http://www.aportainnova.com",
      "company.phone" : "982211446",
      "company.source" : "www.incibe.es",
      "company.id" : "A-porta Innovaciones en Servicios Informáticos",
      "company.name" : "A-porta Innovaciones en Servicios Informáticos",
      "company.logo" : "https://www.incibe.es/sites/default/files/contenidos/catalogo_empresa/logotipoaportainnova.png",
      "company.email" : "info@aportainnova.com"
    }

    # ejemplo de datos contratos
    {
    	"contract.company.name" : "INSIGHT TECHNOLOGY SOLUTIONS, S.L.",
      "contract.cpv" : "48620000",
      "contract.id" : "https://contrataciondelestado.es/sindicacion/licitacionesPerfilContratante/789624",
      "contract.title" : "Licencias de Windows 2008 Server sin assurance",
      "contract.description" : "",
      "contract.company.id" : "B81996605",
      "contract.source" : "www.hacienda.gob.es"
    }

## Licitaciones plataforma PLACE

Este flujo permite la extracción de información de contratos de PLACE.

![](images/place.flow.png)

### **Lectura ficheros**

Este proceso carga los ficheros en las carpetas de entrada (`place/raw` y `place/dumps`) y extrae el contenido xml. Este proceso puede implicar una descompresión de los ficheros de entradas si lo que se está leyendo es un dump.

### División de ficheros por contrato

Cada fichero xml de PLACE tiene una lista de entradas referentes a contratos, este proceso permite a los procesos posteriores trabajar con cada contrato individualmente, dividiendo el fichero de entrada por estas entradas de contratos (elementos `<entry></entry>` en el fichero)

### Extracción de CPVs

Este proceso simplemente lee la entrada del contrato, en formato xml, y extrae algunos campos básico, como el CPV del mismo. La extracción de información se realiza haciendo uso de [XPath queries](https://www.w3schools.com/xml/xpath_syntax.asp).

### Filtro por CVP's

Aquí, se filtran los contratos por sus códigos CPV. El listado de códigos CPV's aceptado puede ser configurado en el fichero `runtime-volumes/nifi/conf/cpvs.white.list.txt`.

### Obtención del objeto del contrato

De la información que se obtiene de la licitación, una de ella es url donde está el documento del pliego del contrato. Con este documento, y haciendo uso del servicio de enriquecimiento, este proceso extrae (siempre que pueda) la información del objeto del contrato contenida en el mismo.

### Extracción de información

Este procesos simplemente añade información adicional, como la fuente de datos, o agrupa los CPV's en un mismo campo.

El flujo puede encontrarse en NiFi en **NiFi Flow >> Contratos PLACE** y presenta la siguiente estructura.

![](images/place.nifi.flow.png)

## Licitaciones de la plataforma europea TED

Este flujo permite la extracción de información de la plataforma contratación europea TED

![](images/ted.flow.png)

### **Lectura ficheros**

Este proceso carga los ficheros en las carpetas de entrada (`ted/raw` y `ted/dumps`) y extrae el contenido xml. Este proceso puede implicar una descompresión de los ficheros de entradas si lo que se está leyendo es un dump.

### Extracción de información

Este proceso extrae información básica necesaria en el resto del flujo. Esta información se corresponde a los códigos CPV de la licitación, así como el tipo de licitación y el formato. La extracción de información se realiza haciendo uso de [XPath queries](https://www.w3schools.com/xml/xpath_syntax.asp).

### Filtro por tipo de documento

En la plataforma place se definen distintas categorías para las licitaciones. Las que contienen la información que nos interesa son de tipo _Award_ (code **7**). Este filtro acepta contrataciones de ese tipo

### Filtro por CVP's

Aquí, se filtran los contratos por sus códigos CPV. El listado de códigos CPV's aceptado puede ser configurado en el fichero `runtime-volumes/nifi/conf/cpvs.white.list.txt`.

### Extracción de información

Este proceso extrae la información a persistir en función del formato de la licitación. La extracción de información se realiza haciendo uso de [XPath queries](https://www.w3schools.com/xml/xpath_syntax.asp).

### Traducción automática

En el proceso anterior se extrae el objeto del contrato. Si no ha sido posible encontrarlo en el documento en castellano, este proceso intenta hacer una traducción automática desde su descripción en inglés (siempre que esté incluida en el documento original).

El flujo puede encontrarse en NiFi en **NiFi Flow >> Contratos TED** y presenta la siguiente estructura.

![](images/ted.nifi.flow.png)

## Contratación latinoamericana (MX)

Este flujo está preparado para extraer información de licitaciones procedentes de la plataforma de datos abiertos del gobierno de México, concretamente del [dataset de procedimientos de contratación de la administración pública federal en formato CSV](https://datos.gob.mx/busca/dataset/concentrado-de-contrataciones-abiertas-de-la-apf/resource/fb232871-24cd-4f93-b4ff-8569ae3ac5ce) y que deberá ser descargado manualmente y ubicar en la carpeta `runtime-volumes/nifi/input/mx/dumps`

Este flujo de datos únicamente extrae la información descriptiva referente al contrato (título y descripción) siempre que esté definida.

El flujo puede encontrarse en NiFi en **NiFi Flow >> Contrataciones MX** y presenta la siguiente estructura.

![](images/mx.nifi.flow.png)

## Indexación del catálogo de empresas (INCIBE)

Este flujo carga el catálogo de empresas, en formato csv, en el sistema de persistencia del servicio. Existe una versión de ese fichero en la ruta `sample-data/incibe/catalogo_empresas.csv` y que debería copiarse a la ruta `runtime-volumes/nifi/input/incibe/raw` para hacer la carga

Este flujo de datos simplemente transforma el catálogo de empresas al modelo que define el servicio

El flujo puede encontrarse en NiFi en NiFi Flow **>>** Catalogo Empresas y presenta la siguiente estructura.

![](images/incibe.nifi.flow.png)

# Servicio de enriquecimiento

El servicio de enriquecimiento expone mediante http JSON tres operaciones de enriquecimiento de información: extracción del objeto del contrato, búsqueda en el registro de dominios y traducción automática del inglés al español.

El servicio requiere tener un listado de dominios descargado. El listado se puede encontrar [aquí](https://sede.red.gob.es/procedimientos/solicitud-acceso-listado-dominios-risp), aunque existe una copia en la ruta `enrichment_service/domains.csv.bz2`

Para información relativa a como lanzar y probar el servicio, puede acceder al readme del mismo en la ruta `enrichment_service/README.md`

## Extracción del objeto del contrato

Esta operación lee el contenido del contrato desde una URI y devuelve el objeto encontrado en el documento.

Esta operación utiliza [apache tika](https://tika.apache.org/) para leer el documento (configurado como servicio docker) y [spaCy](https://spacy.io/) para el procesamiento del texto 


### Ejemplo de uso y respuesta:
```bash
curl -X POST \
http://localhost:8888/document-object-extract \
-H 'Content-Type: application/json' \
-d '{
    "uri": "https://contrataciondelestado.es/wps/wcm/connect/PLACE_es/Site/area/docAccCmpnt?srv=cmpnt&cmpntname=GetDocumentsById&source=library&DocumentIdParam=82f65af7-2451-4987-8dfb-6af584faadd0"
}'

# Respuesta
{ "object": "Cl\u00e1usula 2. Objeto del contrato.  \n \nEl objeto del contrato al que se refiere el presente pliego es la ejecuci\u00f3n de los trabajos descritos en el \napartado 1 del anexo I 1 al mismo y definidos en el pliego de prescripciones t\u00e9cnicas particulares, en \nel que se especifican las necesidades administrativas a satisfacer mediante el contrato y los factores de \ntodo orden a tener en cuenta. \n \nTanto el pliego de prescripciones t\u00e9cnicas particulares como el pliego de cl\u00e1usulas administrativas \nparticulares revisten car\u00e1cter contractual, por lo que deber\u00e1n ser firmados, en prueba de conformidad \npor el adjudicatario, en el mismo acto de formalizaci\u00f3n del contrato \n \nSi el contrato est\u00e1 dividido en lotes, los licitadores podr\u00e1n optar a un lote, a varios o a todos ellos, salvo \nque se establezca un n\u00famero m\u00e1ximo de lotes por licitador, para lo que se estar\u00e1 a lo estipulado en el \napartado 1 del anexo I. \n \n"}
```

## Búsqueda en el registro de dominios

Esta operación permite buscar los dominios registrados de una empresa a partir del nombre o el id (CIF).

Los dominios se buscarán en el listado de dominios proporcionado al arrancar el servicio



### Ejemplo de uso y respuesta:

```bash
curl -X POST \
http://localhost:8888/company-domains \
-H 'Content-Type: application/json' \
-d '{

	  "name": "Mdc S.A."
}'

# Respuesta
["mdc.es"]
```



## Traducción automática

El traductor automático de inglés a español, basado en [OpenNMT](http://opennmt.net/), permite traducir de forma automática los textos en inglés (siempre que sea posible) extraídos de las licitaciones TED.

La traducción está basada en un modelo estadístico, y los resultados pueden no ser totalmente correctos. Este modelo puede ser mejorado con nuevos entrenamientos. Para más información consulte la documentación de OpenNMT-py [aquí](http://opennmt.net/OpenNMT-py/quickstart.html).


### Ejemplo de uso y respuesta:

```bash
curl -X POST \
http://localhost:8888/text-translation \
-H 'Content-Type: application/json' \
-d '{
    "text": "The primary objective of the service contract is to support the 2020 comprehensive review of Member States'' greenhouse gas emission inventories and ensure that the European Commission has accurate, reliable and verified information on annual greenhouse gas emissions for the year 2005 and 2016 to 2018. This information will be used for the following purposes:— to determine the annual emission allocations for the year 2021 to 2030 under the Effort Sharing Regulation for all EU Member States, Iceland and Norway, and— to check Member States'' compliance with their annual targets for the year 2018 under the Effort Sharing Decision.The secondary objective of the service contract is to build capacity of Member State experts with a view to strengthening Member States'' greenhouse gas inventory reporting and preparation of the yearly reporting and compliance cycle."
}'

# Respuesta
{
    "translated": "\" El objetivo fundamental del suministro de servicios de servicios de gases de efecto invernadero para el año 2005 y que la Comisión Europea tiene que ver con la entrada en vigor de las emisiones de gases de efecto invernadero para el año 2005 y que la Comisión Europea tiene la posibilidad de reforzar la revisión global de las emisiones de gases de efecto invernadero para el año 2005 y el cumplimiento de la gestión de los derechos de emisión de los Estados miembros para el año 2005 y el cumplimiento de la gestión de los derechos de emisión de"
}
```


##  Registro de marcas europeas (EUIPO, Oficina de Protección Intelectual de la Unión Europea)

Los datos de la EUIPO están disponibles en [https://euipo.europa.eu/ohimportal/en/open-data](https://euipo.europa.eu/ohimportal/en/open-data)

Están disponibles las descargas desde 2013 a la actualidad.

Se trata de un formato XML y una estructura de carpetas basada en los IDs de los registros.

### Tipología de entidades

EUIPO ofrece datos sobre varios tipos de entidad (en negrita las relevantes para el presente proyecto):
- **Applicants**
- **Trademarks**
- International registrations
- Representatives

### Modelo de datos
El modelo de datos está disponible en los siguientes formatos:

- XSD
- ODP model (Excel)

A continuación, se describen los datos de las entidades de interés para la extracción: Trademarks y Applicants.


### Trademarks

Contiene solicitudes de registro de marca

Registran diferentes operaciones (INSERT, DELETE). 

Solo contienen información de interés operaciones de INSERT:

    <TradeMark operationCode="Insert">

Campos de interés

**Términos productos y servicios**

    <ClassDescription>
       <ClassNumber>3</ClassNumber> <!-- NICE CLASS -->
       <GoodsServicesDescription languageCode="es"> <!-- Terms -->
    		Desinfectantes; Productos farmacéuticos dermatológicos.
    	</GoodsServicesDescription>

**ID de Aplicante**

Solo se indica el Aplicant ID si la marca está en estado PAGADA.

    <ApplicantDetails>
    <ApplicantKey>
    <Identifier>620852</Identifier> <!-- identificador registros dataset Applicants -->
    </ApplicantKey>
    </ApplicantDetails>


### Applicants

Se pueden recuperar aquellos Applicants de marcas registradas y pagadas.

Solo contienen información de interés operaciones de INSERT:

    <Applicant operationCode="Insert">

### Proceso de ETL propuesto para la extracción de marcas y empresas relacionadas con Ciberseguridad

Se propone un proceso de extracción basado en:

1. Recuperación de **registros de Trademark**:
    1. Registro INSERT
    2. Por términos del campo de terminos Productos y Servicios (ver siguiente apartado)
    3. Carga en BBDD de marcas
2. Recuperación registros de **Applicant:**
    1. Extracción de Applicant ID de marcas filtradas
    2. Recuperación registro de Applicant en dataset Applicants (registros INSERT)
    3. Carga en BBDD de Empresas INCIBE.
    
### Recuperación de registros de Trademark
Para el filtrado de marcas relacionadas con Ciberseguridad se utilizará la lista de términos seleccionada por el equipo del proyecto correspondiente al lote 2 
y que se encuentra disponible en este repositorio en `data/listado-terminos-ncl-filtrados.txt`. Para ello se seguirá el siguiente proceso:

1. Filtrar aquellos registros de Trademark cuyo `operationCode="Insert"`.
2. Seleccionar del campo `ClassDescription` aquellos campos `GoodsServicesDescription` cuyo atributo `languageCode="es"`.
3. Separar los términos aplicando un split por `;`.
4. Hacer una comparación de cadenas de carácteres con los términos y si hay un match seleccionar la marca como relacionada con Ciberseguridad.

De estos registros nos interesa sobre todo el campo `Applicant Identifier`, que se utilizará para seleccionar empresas relacionadas con Ciberseguridad.
```xml
<ApplicantDetails>
    <ApplicantKey>
    <Identifier>
```
### Recuperación de registros de Applicant
Se realizará extrayendo aquellos registros de Applicant cuyo `operationCode="Insert"` que aparezcan en la BBDD de marcas creada en el paso anterior. Para ello se utiizará la estructura 
de carpetas encontrada en la descarga de registros de Applicants.
















