# Definition

## Apache Nifi

Integración ETL

### Extract

Para cada tipo de fuente:

#### Mongo

#### Atom

### Transform processors

### Load processors

# Persistance layer

- Elasticsearch

# Setup

Docker + Docker Compose

Environment:

- Local: Local docker service
- Dev: AWS + Docker machine

# Code repositories

1. ETL definitions, conf, custom-components (NIFI). Maven Archetypes
2. NLP service: REST APIs (Open API). Integrate with NIFI `InvokeHTTP`.
