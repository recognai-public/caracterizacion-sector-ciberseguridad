.PHONY: default
default: help 

# Prechecks of needed commands
HAS_DOCKER := $(shell command -v docker;)
HAS_DOCKER_COMPOSE := $(shell command -v docker-compose;)
HAS_PYTHON3 := $(shell command -v python3;)

environment=.venv
source_activate =source $(environment)/bin/activate &&
nifi_input_data = runtime-volumes/nifi/input

bootstrap:
ifndef HAS_PYTHON3
	@echo "No python3 command found. Please install python3 https://www.python.org/downloads/mac-osx/"
	@exit 1
endif
ifndef HAS_DOCKER
	@echo "No docker command found. Please install docker https://docs.docker.com/install/"
	@exit 1
endif

ifndef HAS_DOCKER_COMPOSE
	@echo "No docker-compose command found. Please install docker https://docs.docker.com/compose/install/"
	@exit 1
endif
	@python3 -m venv $(environment)

init: bootstrap ## Prepare a python virtual environments, install dependencies an so on. Do it just once
	@$(source_activate) pip install -U pip && pip install -r nifi-extensions/scripts/requirements.txt
			
package: ## Build custom nifi procesors
	@mvn -f nifi-extensions/pom.xml clean package

build: package ## Build the needed docker images (depends on package directive)
	@mkdir -p ${nifi_input_data}/place/dumps
	@mkdir -p ${nifi_input_data}/place/raw
	@mkdir -p ${nifi_input_data}/ted/dumps
	@mkdir -p ${nifi_input_data}/ted/raw
	@mkdir -p ${nifi_input_data}/incibe/raw
	@mkdir -p ${nifi_input_data}/mx/raw
	@mkdir -p ${nifi_input_data}/mx/dumps
	@mkdir -p ${nifi_input_data}/euipo/applicants/dumps
	@mkdir -p ${nifi_input_data}/euipo/trademarks/dumps
	@docker-compose build 

productos-servicios: ## Load data for services catalog
ifndef file
	@echo "Debe indicar el fichero a cargar: make productos-servicios file=ruta/al/fichero.csv"
	@exit 1
endif
	@$(source_activate) python nifi-extensions/scripts/csv_to_elastic.py \
	--elastic-address localhost:9200 \
	--elastic-index productos-servicios \
	--elastic-type _doc \
	--csv-file $(file)

start: ## Launch local services
	@docker-compose up -d

stop: ## Stop local services
	@docker-compose stop

status: ## Check services status
	@docker-compose ps

scale-up: ## Increments instances for enrichment processing performance (Launching multiple times has no effect)
	@docker-compose up -d --scale enrichment=2 --scale tika-server=4

scale-down: ## Reduce instances for enrichment processing performance (Launching multiple times has no effect)
	@docker-compose up -d --scale enrichment=1 --scale tika-server=1

logs: ## Attach services logs
	@docker-compose logs -f --tail=500

.PHONY: help
help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
